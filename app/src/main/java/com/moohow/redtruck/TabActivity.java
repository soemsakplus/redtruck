package com.moohow.redtruck;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

public class TabActivity extends AppCompatActivity
        implements PassengerFragment.OnFragmentInteractionListener,
        DriverFragment.OnFragmentInteractionListener,
        HelpFragment.OnFragmentInteractionListener{

    BottomNavigationView navigation;

    // git test
    PassengerFragment passengerFragment;
    DriverFragment driverFragment;
    HelpFragment helpFragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if(passengerFragment == null) {
                        passengerFragment = new PassengerFragment();
                        passengerFragment.setArguments(getIntent().getExtras());
                    }
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, passengerFragment)
                            .commit();
                    return true;
                case R.id.navigation_dashboard:
                    if(driverFragment == null) {
                        driverFragment = new DriverFragment();
                        driverFragment.setArguments(getIntent().getExtras());
                    }
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, driverFragment)
                            .commit();
                    return true;
                case R.id.navigation_notifications:
                    if(helpFragment == null) {
                        helpFragment = new HelpFragment();
                        helpFragment.setArguments(getIntent().getExtras());
                    }
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, helpFragment)
                            .commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        passengerFragment = new PassengerFragment();
        passengerFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, passengerFragment)
//                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
